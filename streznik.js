var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	var davcnaStevilka = req.query.ds;
	var ime = req.query.ime;
	var priimek = req.query.priimek;
	var ulica = req.query.ulica;
	var hisnaStevilka = req.query.hisnaStevilka;
	var postnaStevilka = req.query.postnaStevilka;
	var kraj = req.query.kraj;
	var drzava = req.query.drzava;
	var poklic = req.query.poklic;
	var telefonskaStevilka = req.query.telefonskaStevilka;

	if (davcnaStevilka == undefined || ime == undefined || priimek == undefined || ulica == undefined || hisnaStevilka == undefined || postnaStevilka == undefined || kraj == undefined || drzava == undefined || poklic == undefined || telefonskaStevilka == undefined) {
		res.send('Napaka pri dodajanju osebe!');
	} else {
		var zeObstaja = false;
		for (i in uporabnikiSpomin) {
			if (uporabnikiSpomin[i].davcnaStevilka == davcnaStevilka) {
				zeObstaja = true;
				break;
			}
		}
		if (!zeObstaja) {
			uporabnikiSpomin.push({
				davcnaStevilka: davcnaStevilka, 
				ime: ime, 
				priimek: priimek, 
				naslov: ulica, 
				hisnaStevilka: hisnaStevilka, 
				postnaStevilka: postnaStevilka, 
				kraj: kraj, 
				drzava: drzava, 
				poklic: poklic, 
				telefonskaStevilka: telefonskaStevilka
			});
			res.redirect('/');
		} else {
			res.send('Oseba z davčno številko ' + davcnaStevilka + ' že obstaja!' + '<br/>' + '<a href="javascript:window.history.back()">Nazaj</a>');
		}
	}
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
	var davcnaSt = req.param('id');
	
	if(!davcnaSt) {
 		return res.send("Napačna zahteva!");
 	}
 	
	var id = najdi(davcnaSt);
	
	if (id !== -1) {
		uporabnikiSpomin.splice(id, 1);
		res.redirect('/');
	}
	
	else {
 		res.send("Oseba z davčno številko " + davcnaSt + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
 	}
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];

function najdi(stevilka) {
	if(stevilka) {
		for (var i = 0; i < uporabnikiSpomin.length; i++) {
			if (uporabnikiSpomin[i].davcnaStevilka === stevilka) {
				return i;
			}
		}
	}
	return -1;
}